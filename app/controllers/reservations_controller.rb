class ReservationsController < ApplicationController
  def create
    ActiveRecord::Base.transaction do
      payload = payload(params)

      # check if reservation is exist
      reservation = Reservation.find_by(code: payload[:reservation][:code])
      if reservation.present?

        @reservation = reservation.update(payload[:reservation])

        # check if guest if exist
        guest = Guest.find(reservation.guest_id)
        if guest.present?
          @guest = guest.update(payload[:guest])
        end

        render :json => { guest: guest, reservation: reservation }, :status => :accepted

      else

        @guest = Guest.new(payload[:guest])
    
        if @guest.save
    
          @reservation = Reservation.new(payload[:reservation].merge(guest_id: @guest.id))
    
          if @reservation.save
            render :json => { guest: @guest, reservation: @reservation }, :status => :created
          else
            render :json => @reservation.errors, :status => :unprocessable_entity
          end
    
        else
          render :json => @guest.errors, :status => :unprocessable_entity
        end

      end

    end
  end

  private
    def payload(params)
      payload1 = params
      payload2 = params[:reservation]

      is_payload2 = payload2.key?(:code)

      # Reservation
      code = is_payload2 ? payload2[:code] : payload1[:reservation_code]
      start_date = is_payload2 ? payload2[:start_date] : payload1[:start_date]
      end_date = is_payload2 ? payload2[:end_date] : payload1[:end_date]
      nights = is_payload2 ? payload2[:nights] : payload1[:nights]
      guests = is_payload2 ? payload2[:number_of_guests] : payload1[:guests]
      adults = is_payload2 ? payload2[:guest_details][:number_of_adults] : payload1[:adults]
      children = is_payload2 ? payload2[:guest_details][:number_of_children] : payload1[:children]
      infants = is_payload2 ? payload2[:guest_details][:number_of_infants] : payload1[:infants]
      status = is_payload2 ? payload2[:status_type] : payload1[:status]
      currency = is_payload2 ? payload2[:host_currency] : payload1[:currency]
      payout_price = is_payload2 ? payload2[:expected_payout_price] : payload1[:payout_price]
      security_price = is_payload2 ? payload2[:listing_security_price_accurate] : payload1[:security_price]
      total_price = is_payload2 ? payload2[:total_paid_amount_accurate] : payload1[:total_price]

      # Guest
      guest_first_name = is_payload2 ? payload2[:guest_first_name] : payload1[:guest][:first_name]
      guest_last_name = is_payload2 ? payload2[:guest_last_name] : payload1[:guest][:last_name]
      guest_phone = is_payload2 ? payload2[:guest_phone_number].uniq&.first : payload1[:guest][:phone]
      guest_email = is_payload2 ? payload2[:guest_email] : payload1[:guest][:email]

      guest = {
        first_name: guest_first_name,
        last_name: guest_last_name,
        phone: guest_phone,
        email: guest_email
      }

      reservation = {
        code: code,
        start_date: start_date,
        end_date: end_date,
        nights: nights,
        guests: guests,
        adults: adults,
        children: children,
        infants: infants,
        status: status,
        currency: currency,
        payout_price: payout_price,
        security_price: security_price,
        total_price: total_price
      }

      return { guest: guest, reservation: reservation }
    end
end
