class CreateReservation < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.timestamps

      t.references :guest, foreign_key: true
      t.string :code
      t.date :start_date
      t.date :end_date
      t.integer :nights
      t.integer :guests
      t.integer :adults
      t.integer :children
      t.integer :infants
      t.string :status
      t.string :currency
      t.decimal :payout_price, precision: 26, scale: 2
      t.decimal :security_price, precision: 26, scale: 2
      t.decimal :total_price, precision: 26, scale: 2
    end
    add_index :reservations, :code, unique: true
  end
end
