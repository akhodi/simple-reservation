class CreateGuest < ActiveRecord::Migration[6.0]
  def change
    create_table :guests do |t|
      t.timestamps

      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :email
    end
    add_index :guests, :email, unique: true
  end
end
