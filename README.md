## Tech Stack

- You can see on [Gemfile](Gemfile)

## Requirements

- Ruby (recommendations for using `rbenv`)
- Rails
- PostgreSQL

## Setup

#### Quick Setup / Re-Setup:
```
rails db:migrate
```

#### Development Environment:
```bash
bundle install
```

#### Test Environment:
```bash
bundle exec rspec
```

#### Secret Key Base:
```bash
rails secret
```

#### Run Web Server (puma)
```
rails s
```
