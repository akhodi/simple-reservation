require 'rails_helper'

RSpec.describe ReservationsController, type: :routing do
  describe 'routing' do
    it 'routes to #create' do
      expect(:post => '/reservations').to route_to('reservations#create')
    end
  end
end
