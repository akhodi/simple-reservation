require 'rails_helper'

RSpec.describe Reservation, type: :model do
  it { should validate_presence_of(:code) }
  it { should validate_presence_of(:start_date) }
  it { should validate_presence_of(:end_date) }
  it { should validate_presence_of(:nights) }
  it { should validate_presence_of(:guests) }
  it { should validate_presence_of(:adults) }
  it { should validate_presence_of(:children) }
  it { should validate_presence_of(:infants) }
  it { should validate_presence_of(:status) }
  it { should validate_presence_of(:currency) }
  it { should validate_presence_of(:payout_price) }
  it { should validate_presence_of(:security_price) }
  it { should validate_presence_of(:total_price) }
end
