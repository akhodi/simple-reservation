require 'rails_helper'

RSpec.describe "Reservations", type: :request do
  let(:valid_attributes) do
    {
      reservation: {
        code: "XXX12345678",
        start_date: "2021-03-12",
        end_date: "2021-03-16",
        expected_payout_price: "3800.00",
        guest_details: {
          localized_description: "4 guests",
          number_of_adults: 2,
          number_of_children: 2,
          number_of_infants: 0  
        },
        guest_email: "wayne_woodbridge@bnb.co.id",
        guest_first_name: "Wayne",
        guest_last_name: "Woodbridge",
        guest_phone_number: [
          "639123456789",
          "639123456789"
        ],
        listing_security_price_accurate: "500.00",
        host_currency: "AUD",
        nights: 4,
        number_of_guests: 4,
        status_type: "accepted",
        total_paid_amount_accurate:  "4300.00"
      }
    }.to_h
  end

  describe 'POST /reservations' do
    context 'and valid params' do
      before { post reservations_path, params: valid_attributes }

      it 'should have http status 201' do
        expect(response).to have_http_status(201)
      end
    end
  end

end
